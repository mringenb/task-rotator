# task-rotator

A simple task list that randomly picks from a list of tasks with replacement when a task is complete.
Rewards are given for completing tasks.

https://mringenb.gitlab.io/task-rotator/

## Support
Use this repository's issue tracker to submit bugs.

## Roadmap
Use this repository's issue tracker to suggest new features.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
@mringenb

## License
MIT

## Project status
Early development.  Mostly this was a day's work to help my spouse get things done and any additional development will be based on user feedback.
